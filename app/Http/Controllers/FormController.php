<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form() {
        return view('page.form');
    }
    public function kirim(Request $request){
        $fullname = $request->fullname;
        $alamat = $request->address;
        return view('page.selamat', compact('fullname', 'alamat'));  

    }
}
