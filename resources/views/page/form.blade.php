@extends('layout.master')

@section('judul')
    Halaman Pendataan   
@endsection

@section('isi') 

    <form action="/post" method="POST"> 
        @csrf
        <label>Nama</label><br>
        <input type="text" name="fullname"><br>
        <label>Alamat</label><br> 
        <textarea name="address" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Kirim">
    </form>
    
@endsection