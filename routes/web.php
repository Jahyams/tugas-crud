<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomeController@home');

Route::get('/form', 'FormController@form');

Route::post('/post', 'FormController@Kirim');

Route::get('/datatable', function (){
    return view('table.datatable');
});

Route::get('/table', function (){
    return view('table.table');
});

Route::get('/cast', 'castController@index');
Route::get('/cast/create', 'castController@create');
Route::post('/cast', 'castController@store');
Route::get('/cast/{cast_id}', 'castController@show');
Route::get('/cast/{cast_id}/edit', 'castController@edit');
Route::put('/cast/{cast_id}', 'castController@update');
Route::delete('/cast/{cast_id}', 'castController@destroy');